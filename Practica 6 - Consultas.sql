﻿USE practica6;

/* Consulta 1 */
SELECT 
  a.titulo_art 
  FROM 
    tema t
  JOIN
    articulo a USING(codTema)
  WHERE
    t.descripcion='Base de Datos'
  AND
    a.anio='1990';

-- Optimizada
SELECT 
  c2.titulo_art 
  FROM 
    (
      -- C1
      SELECT 
        t.codTema 
        FROM 
          tema t
        WHERE
          t.descripcion='Base de Datos'
    ) c1
  JOIN
    (
      -- C2
      SELECT 
        a.codTema,a.titulo_art 
        FROM 
          articulo a
        WHERE
          a.anio='1990'
    ) c2 USING(codTema);

/* Consulta 2 */
SELECT 
  r.titulo_rev 
  FROM 
    revista r
  JOIN
    (
      SELECT 
        c1.referencia 
        FROM
          (
            SELECT 
              a.referencia,a.codTema 
              FROM 
                articulo a 
          ) c1
        GROUP BY 
          c1.referencia
        HAVING
          COUNT(*)=
          (
            SELECT 
              COUNT(*) 
              FROM 
                tema t              
          )          
    ) c2 USING(referencia);

-- Optimizada
SELECT 
  c1.titulo_rev 
  FROM 
    ( 
      -- C1
      SELECT 
      r.referencia,r.titulo_rev 
      FROM  
        revista r
    ) c1
  JOIN
    (
      -- C2
      SELECT 
        c1.referencia 
        FROM
          (
            SELECT 
              a.referencia,a.codTema 
              FROM 
                articulo a 
          ) c1
        GROUP BY 
          c1.referencia
        HAVING
          COUNT(*)=
          (
            -- C2a
            SELECT 
              COUNT(*) 
              FROM 
                tema t              
          )          
    ) c2 USING(referencia);


/* Consulta 3 */
SELECT 
  r.titulo_rev 
  FROM
    revista r
  JOIN
    (
      SELECT 
        c1.referencia
        FROM 
          revista r
        LEFT JOIN
          (
            SELECT 
              a.referencia 
              FROM 
                tema t 
              JOIN
                articulo a USING(codTema)
              WHERE
                t.descripcion<>'MEDICINA'
          ) c1 USING(referencia)
        WHERE
          c1.referencia IS NULL        

    ) c2 USING(referencia);


-- Optimizada
SELECT 
  c1.titulo_rev 
  FROM
    (
      -- C1
      SELECT 
        r.referencia,r.titulo_rev 
        FROM 
          revista r
    ) c1   
  JOIN
    (
      SELECT 
        c2.referencia
        FROM 
          ( 
            -- C2a
            SELECT 
              r.referencia 
              FROM  
                revista r) c2a
        LEFT JOIN
          (
            -- C2
            SELECT 
              c2b.referencia 
              FROM 
                tema t 
              JOIN
                (
                  -- C2b
                  SELECT 
                    a.codTema,a.referencia 
                    FROM  
                      articulo a 
                ) c2b USING(codTema)
              WHERE
                t.descripcion<>'MEDICINA'
          ) c2 USING(referencia)
        WHERE
          c2.referencia IS NULL        
    ) c2 USING(referencia);

/* Consulta 4 */
SELECT 
  c1.nombre
  FROM 
    (
      SELECT 
        *
        FROM
          autor a
        JOIN
          (
            SELECT 
              a.dni
              FROM 
                articulo a
              JOIN
                tema t USING(codTema)
              WHERE
                a.anio='1991'
              AND
                t.descripcion='SQL'
          ) c1 USING(dni)
    ) c1
  NATURAL JOIN
    (
      SELECT 
        a.dni 
        FROM 
          articulo a
        JOIN 
          tema t USING(codTema) 
        WHERE
          a.anio='1992'
        AND
          t.descripcion='SQL'
    ) c2;

-- Optimizada
SELECT 
  c1.dni
  FROM 
    (
      -- C1
      SELECT 
        c1a.dni
        FROM
          (
            -- C1a
            SELECT 
              a.dni 
              FROM 
                autor a
          ) c1a
        JOIN
          (
            -- C1c
            SELECT 
             c1b.dni
              FROM 
                (
                  -- C1b
                  SELECT 
                    a.dni,a.codTema,a.anio 
                    FROM 
                      articulo a
                ) c1b
              JOIN
                tema t USING(codTema)
              WHERE
                c1b.anio='1991'
              AND
                t.descripcion='SQL'
          ) c1c USING(dni)
    ) c1
  NATURAL JOIN    
    (
      -- C2
      SELECT 
        c2a.dni 
        FROM
          (
            -- C2a
            SELECT 
              a.codTema,dni,a.anio 
              FROM  
                articulo a
          ) c2a
        JOIN 
          tema t USING(codTema) 
        WHERE
          c2a.anio='1992'
        AND
          t.descripcion='SQL'
    ) c2;

/* Consulta 5 */
SELECT 
  a.titulo_art
  FROM
    articulo a
  JOIN
    autor a1 USING(dni)
  WHERE
    a.anio='1993' 
  AND 
    a1.universidad='Politecnica de Madrid';

-- Optimizada
SELECT 
  c1.titulo_art
  FROM
    (
      -- C1
      SELECT 
        dni,a.titulo_art 
        FROM  
          articulo a
        WHERE 
          a.anio='1993'
    ) c1
  JOIN
    (
      -- C2
      SELECT 
        a.dni 
        FROM 
          autor a 
        WHERE 
          a.universidad='Politecnica de Madrid'
    ) c2 USING(dni);
   

