﻿DROP DATABASE IF EXISTS practica6;
CREATE DATABASE IF NOT EXISTS practica6;

USE practica6;

CREATE OR REPLACE TABLE autor(
  dni varchar(11),
  nombre varchar(30),
  universidad varchar(30),
  PRIMARY KEY(dni)
  );

CREATE OR REPLACE TABLE tema(
  codTema varchar(5),
  descripcion text,
  PRIMARY KEY(codTema)
  );

CREATE OR REPLACE TABLE revista(
  referencia varchar(5),
  titulo_rev varchar(25),
  editorial varchar(40),
  PRIMARY KEY(referencia)
  );

CREATE OR REPLACE TABLE articulo(
  referencia varchar(5),
  dni varchar(11),
  codTema varchar(5),
  titulo_art varchar(50),
  anio year,
  volumen varchar(5),
  numero int,
  paginas int,
  PRIMARY KEY(referencia,dni,codTema)
  );

ALTER TABLE articulo  
  ADD 
    CONSTRAINT fkRevistaArticulo FOREIGN KEY (referencia)
      REFERENCES revista(referencia) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD
    CONSTRAINT fkAutorArticulo FOREIGN KEY (dni)
      REFERENCES autor(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD
    CONSTRAINT fkTemaArticulo FOREIGN KEY (codTema)
      REFERENCES tema(codTema) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO autor (dni, nombre, universidad)
  VALUES ('12345678A', 'Autor 1', 'Politecnica de Madrid'),
         ('12345678B', 'Autor 2', 'Politecnica de Sevilla'),
         ('12345678C', 'Autor 3', 'Politecnica de Cantabria');

INSERT INTO tema (codTema, descripcion)
  VALUES ('TM001', 'Medicina'),
         ('TM002', 'Base de Datos'),
         ('TM003', 'SQL');

INSERT INTO revista (referencia, titulo_rev, editorial)
  VALUES ('ref01', 'titulo revista 1', 'editorial 1'),
         ('ref02', 'titulo revista 2', 'editorial 2'),
         ('ref03', 'titulo revista 3', 'editorial 3');

INSERT INTO articulo (referencia, dni, codTema, titulo_art, anio, volumen, numero, paginas)
  VALUES ('ref01', '12345678B', 'TM001', 'titulo Articulo 1', '1990', 'I', 100, 250),
         ('ref02', '12345678A', 'TM002', 'titulo Articulo 2', '1991', 'I', 100, 250),
         ('ref03', '12345678C', 'TM003', 'titulo Articulo 3', '1993', 'III', 80, 150);


